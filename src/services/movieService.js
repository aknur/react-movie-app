import axios from 'axios';

const apiKey = 'f79aeba3';

const api = axios.create({
  baseURL: `http://www.omdbapi.com/?apikey=${apiKey}`,
});

export function searchMovie(searchText, page = 1) {
  return api.get('', {
    params: {
      s: searchText,
      page,
    },
  });
}

export function getMovie(imdbID) {
  return api.get('', {
    params: {
      i: imdbID,
    },
  });
}
