import React, { useEffect, useState } from 'react';
import Skeleton from '../../components/skeleton/Skeleton';
import { getMovie } from '../../services/movieService';

import './MoviePage.scss';
import clockImg from '../../images/clock.png';
import maskImg from '../../images/mask.png';
import starImg from '../../images/star.png';

const MoviePage = props => {
  const [movie, setMovie] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const imdbID = props.match.params.id;
    setLoading(true);

    getMovie(imdbID)
      .then(({ data }) => {
        if (data.Response === 'True') {
          setMovie(data);
        } else {
          setMovie(null);
        }

        setLoading(false);
      })
      .catch(error => {
        setLoading(false);
        setMovie(null);
      });

    return () => {};
  }, [props.match]);

  return (
    <div className="MoviePage">
      {loading ? (
        <>
          <Skeleton className="MoviePage__skeleton__poster" />
          <div className="MoviePage__description">
            <Skeleton className="MoviePage__skeleton__title" />
            <Skeleton className="MoviePage__skeleton__plot" />
            <div className="MoviePage__detail">
              <img src={starImg} alt="time" />
              <Skeleton className="MoviePage__skeleton__detail" />
            </div>
            <div className="MoviePage__detail">
              <img src={clockImg} alt="time" />
              <Skeleton className="MoviePage__skeleton__detail" />
            </div>
            <div className="MoviePage__detail">
              <img src={maskImg} alt="time" />
              <Skeleton className="MoviePage__skeleton__detail" />
            </div>
            <Skeleton className="MoviePage__skeleton__plot" />
          </div>
        </>
      ) : movie ? (
        <>
          <img className="MoviePage__poster" src={movie.Poster} alt="poster" />
          <div className="MoviePage__description">
            <h1 className="MoviePage__title">{movie.Title}</h1>
            <p className="MoviePage__detail">
              {movie.Year} {movie.Type}
            </p>
            <p className="MoviePage__detail">
              <img src={starImg} alt="time" />
              {movie.imdbRating} IMDB
            </p>
            <p className="MoviePage__detail">
              <img src={clockImg} alt="time" />
              {movie.Runtime}
            </p>
            <p className="MoviePage__detail">
              <img src={maskImg} alt="time" />
              {movie.Genre}
            </p>
            <p>{movie.Plot}</p>
          </div>
        </>
      ) : (
        <p className="error">Sorry, movie not found</p>
      )}
    </div>
  );
};

export default MoviePage;
