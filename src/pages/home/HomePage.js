import React from 'react';

import './HomePage.scss';
import homeImg from '../../images/may_the_force_bgdm.svg';

const HomePage = () => {
  return (
    <div className="HomePage">
      <img src={homeImg} alt="May the force be with you" className="Home__image" />
    </div>
  );
};

export default HomePage;
