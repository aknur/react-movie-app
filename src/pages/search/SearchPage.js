import React, { useState, useEffect } from 'react';
import MoviesList from '../../components/movies-list/MoviesList';
import Pagination from '../../components/pagination/Pagination';

import { withRouter } from 'react-router-dom';
import queryString from 'query-string';

import { searchMovie } from '../../services/movieService';

const SearchPage = ({ location }) => {
  const [movies, setMovies] = useState([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(queryString.parse(location.search).page);

  const [error, setError] = useState('');
  const [pagesTotal, setPagesTotal] = useState(1);

  useEffect(() => {
    const loadMovies = () => {
      setLoading(true);

      const query = queryString.parse(location.search);
      searchMovie(query.searchText, query.page).then(response => {
        const searchResult = response.data;

        if (searchResult.Response === 'True') {
          const total = Math.ceil(searchResult.totalResults / 10);

          setPagesTotal(total);
          setMovies(searchResult.Search);
          setError('');
          setPage(query.page);
        } else {
          setMovies([]);
          setError(searchResult.Error);
        }

        setLoading(false);
      });
    };

    loadMovies();

    try {
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth',
      });
    } catch (error) {
      window.scrollTo(0, 0);
    }

    return () => {
      setMovies([]);
      setError('');
    };
  }, [location]);

  const getLink = () => {
    const query = queryString.parse(location.search);
    delete query.page;
    return `/search?${queryString.stringify(query)}`;
  };

  return (
    <>
      <MoviesList movies={movies} loading={loading} />
      {movies.length ? (
        <Pagination link={getLink()} total={+pagesTotal} currentPage={+page} />
      ) : null}
      {error ? <p className="error">{error}</p> : null}
    </>
  );
};

export default withRouter(SearchPage);
