import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import SearchPage from './pages/search/SearchPage';
import SearchBar from './components/search-bar/SearchBar';
import MoviePage from './pages/movie/MoviePage';
import Header from './components/header/Header';
import HomePage from './pages/home/HomePage';

function App() {
  return (
    <>
      <Router>
        <Header />
        <SearchBar />
        <div className="container">
          <Switch>
            <Route path="/" exact component={HomePage} />
            <Route path="/search" component={SearchPage} />
            <Route path="/movie/:id" component={MoviePage} />
          </Switch>
        </div>
      </Router>
    </>
  );
}

export default App;
