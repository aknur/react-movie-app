import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import './MovieCard.scss';

const MovieCard = ({ movie }) => {
  const [imageLoaded, setImageLoaded] = useState(false);
  const [showImage, setShowImage] = useState(true);

  return (
    <div className="MovieCard">
      <Link to={`/movie/${movie.imdbID}`}>
        <div
          className={`MovieCard__poster ${imageLoaded ? 'MovieCard__poster--loaded' : ''} ${
            showImage ? '' : 'MovieCard__poster--hidden'
          }`}
        >
          <img
            src={movie.Poster}
            alt={movie.Title}
            onLoad={() => setImageLoaded(true)}
            onError={() => setShowImage(false)}
          />
        </div>
        <p className="MovieCard__title">{movie.Title}</p>
        <p className="MovieCard__subtitle">{movie.Year}</p>
        <p className="MovieCard__subtitle">{movie.Type}</p>
      </Link>
    </div>
  );
};

export default MovieCard;
