import React, { useEffect, useState } from 'react';

import queryString from 'query-string';
import { withRouter } from 'react-router-dom';

import './SearchBar.scss';

import searchImg from '../../images/search.png';
import backImg from '../../images/back.png';

const SearchBar = ({ location, history }) => {
  const [searchText, setSearchText] = useState('');
  const [error, setError] = useState('');
  const [isBackShown, setIsBackShown] = useState(false);

  useEffect(() => {
    if (location.pathname === '/') {
      setSearchText('');
      setError('');
    } else if (location.search) {
      const query = queryString.parse(location.search);
      setSearchText(query.searchText);
    }

    if (location.pathname === '/') {
      setIsBackShown(false);
    } else {
      setIsBackShown(true);
    }

    return () => {};
  }, [location]);

  const handleChange = event => setSearchText(event.target.value);

  const handleSubmit = event => {
    event.preventDefault();
    if (validate()) {
      const link = `/search?searchText=${searchText}&page=1`;
      history.push(link);
    }
  };

  const validate = () => {
    if (searchText.length < 3) {
      setError('Few more symbols please');
      return false;
    }

    setError('');
    return true;
  };

  return (
    <div className="SearchBar">
      <form className="SearchBar__form" onSubmit={handleSubmit}>
        {isBackShown && (
          <button type="button" onClick={history.goBack} className="SearchBar__back">
            <img src={backImg} alt="back" />
          </button>
        )}
        <div className="SearchBar__input-group">
          <input className="SearchBar__input" value={searchText} onChange={handleChange} />
          {error && <p className="SearchBar__input__error">{error}</p>}
        </div>
        <button className="SearchBar__button" type="submit">
          <img src={searchImg} alt="search" />
        </button>
      </form>
    </div>
  );
};

export default withRouter(SearchBar);
