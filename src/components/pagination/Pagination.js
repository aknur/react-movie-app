import React from 'react';
import { Link } from 'react-router-dom';

import './Pagination.scss';
import leftImg from '../../images/left.png';
import rightImg from '../../images/right.png';

const Pagination = ({ link, total, currentPage }) => {
  const pageNumbers = [];
  let hasLeft = false;
  let hasRight = true;
  let from = 1;
  let to = 5;

  if (currentPage !== 1) {
    hasLeft = true;
  }

  if (currentPage > 2) {
    to = currentPage + 2;
    from = currentPage - 2;
  }

  if (currentPage + 2 > total) {
    to = total;
    hasRight = true;
  }

  if (currentPage >= total - 2) {
    from = total - 4;
  }

  for (let i = from; i <= to; i++) {
    pageNumbers.push(i);
  }

  return (
    <ul className="Pagination">
      {hasLeft && (
        <Link to={`${link}&page=${currentPage - 1}`}>
          <li className="Pagination__item">
            <img src={leftImg} alt="Left" />
          </li>
        </Link>
      )}
      {pageNumbers.map(number => (
        <Link key={number} to={`${link}&page=${number}`}>
          <li
            className={`Pagination__item ${
              number === currentPage ? 'Pagination__item--active' : ''
            }`}
          >
            {number}
          </li>
        </Link>
      ))}
      {hasRight && (
        <Link to={`${link}&page=${currentPage + 1}`}>
          <li className="Pagination__item">
            <img src={rightImg} alt="Right" />
          </li>
        </Link>
      )}
    </ul>
  );
};

export default Pagination;
