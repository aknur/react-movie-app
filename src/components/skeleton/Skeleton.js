import React from 'react';
import styled from 'styled-components';

const SkeletonInner = styled.div`
  width: 100%;
  height: 100%;
  background-color: #444;
  transition: 200ms;
`;

function Skeleton({ className }) {
  return (
    <div className={className}>
      <SkeletonInner />
    </div>
  );
}

export default Skeleton;
