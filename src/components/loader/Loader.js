import React from 'react';
import './Loader.scss';

function Loader() {
  return <div className="Loader">Loading...</div>;
}

export default Loader;
