import React from 'react';

import './MovieCardSkeleton.scss';
import Skeleton from '../skeleton/Skeleton';

export default function MovieCardSkeleton() {
  return (
    <div className="MovieCardSkeleton">
      <Skeleton className="MovieCardSkeleton__poster" />
      <Skeleton className="MovieCardSkeleton__title" />
      <Skeleton className="MovieCardSkeleton__info" />
      <Skeleton className="MovieCardSkeleton__info" />
    </div>
  );
}
