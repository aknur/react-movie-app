import React from 'react';
import { Link } from 'react-router-dom';

import './Header.scss';
import logo from '../../images/cinema.png';

const Header = () => {
  return (
    <header>
      <Link to="/">
        <img src={logo} alt="movies" className="logo" />
      </Link>
    </header>
  );
};

export default Header;
