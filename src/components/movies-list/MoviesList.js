import React from 'react';

import './MoviesList.scss';
import MovieCard from '../movie-card/MovieCard';
import MovieCardSkeleton from '../movie-card-skeleton/MovieCardSkeleton';

function MoviesList({ movies, loading }) {
  const movieSkeletons = [];
  for (let i = 0; i < 10; i++) {
    movieSkeletons.push(i);
  }

  return (
    <div className="MoviesList">
      {loading
        ? movieSkeletons.map(skeleton => <MovieCardSkeleton key={skeleton} />)
        : movies.length
        ? movies.map(movie => <MovieCard key={movie.imdbID} movie={movie} />)
        : null}
    </div>
  );
}

export default MoviesList;
